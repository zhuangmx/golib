package orm

import "database/sql"

type TransactionContext struct {
	Db *sql.DB
	Tx *sql.Tx
}

func (transactionContext *TransactionContext) Begin() (*TransactionContext, error) {
	tx, err := transactionContext.Db.Begin()
	transactionContext.Tx = tx
	return transactionContext, err
}

func (transactionContext *TransactionContext) Rollback() {
	_ = transactionContext.Tx.Rollback()
}

func (transactionContext *TransactionContext) Commit() error {
	return transactionContext.Tx.Commit()
}
