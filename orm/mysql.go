package orm

import (
	"fmt"
	"github.com/gookit/goutil/mathutil"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"os"
	"time"
)

type OpTime struct {
	CreatedAt time.Time      `json:"createdAt" gorm:"<-:create;type:datetime;;default null"`  //新增时间
	UpdatedAt time.Time      `json:"updatedAt" gorm:"type:datetime;default null"`             //修改时间
	DeletedAt gorm.DeletedAt `json:"deletedAt" gorm:"type:datetime;default null" sql:"index"` //删除时间
}

var DefaultConnection *MySQLConnect

type MySQLConnect struct {
	Host        string `json:"host" yaml:"host"`
	Port        string `json:"port" yaml:"port"`
	User        string `json:"user" yaml:"user"`
	Password    string `json:"password" yaml:"password"`
	DataBase    string `json:"database" yaml:"database"`
	Prefix      string `json:"prefix" yaml:"prefix"`
	Debug       string `json:"debug" yaml:"debug"`
	MaxIdleConn string `json:"maxIdle" yaml:"maxIdleConn"`
	MaxOpenConn string `json:"maxOpenConn" yaml:"maxOpenConn"`
	MaxLifeTime string `json:"maxLifeTime" yaml:"maxLifeTime"`
}

var ormDb *gorm.DB

func newConnect(connection *MySQLConnect) *gorm.DB {
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local",
		connection.User,
		connection.Password,
		connection.Host,
		connection.Port,
		connection.DataBase,
	)
	var err error
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
			TablePrefix:   connection.Prefix,
		},
	})
	if err != nil {
		fmt.Println("connect mysql error:", err)
	}
	sqlDb, _ := db.DB()
	sqlDb.SetMaxIdleConns(mathutil.MustInt(connection.MaxIdleConn))
	sqlDb.SetMaxOpenConns(mathutil.MustInt(connection.MaxOpenConn))
	sqlDb.SetConnMaxLifetime(time.Duration(mathutil.MustInt(connection.MaxLifeTime)) * time.Minute)
	ormDb = db
	return ormDb
}

func NewDefault() *gorm.DB {
	if ormDb == nil {
		if DefaultConnection == nil {
			DefaultConnection = &MySQLConnect{
				Host:        os.Getenv("MYSQL_HOST"),
				Port:        os.Getenv("MYSQL_PORT"),
				User:        os.Getenv("MYSQL_USER"),
				Password:    os.Getenv("MYSQL_PASSWORD"),
				DataBase:    os.Getenv("MYSQL_DATABASE"),
				Prefix:      os.Getenv("MYSQL_PREFIX"),
				Debug:       os.Getenv("MYSQL_DEBUG"),
				MaxIdleConn: os.Getenv("MYSQL_MAXIDLECONN"),
				MaxOpenConn: os.Getenv("MYSQL_MAXOPENCONN"),
				MaxLifeTime: os.Getenv("MYSQL_MAXLIFETIME"),
			}
			if DefaultConnection.MaxIdleConn == "" {
				DefaultConnection.MaxIdleConn = "20"
			}
			if DefaultConnection.MaxOpenConn == "" {
				DefaultConnection.MaxOpenConn = "100"
			}
			if DefaultConnection.MaxLifeTime == "" {
				DefaultConnection.MaxLifeTime = "1"
			}
			newConnect(DefaultConnection)
		}
	}
	return ormDb
}
