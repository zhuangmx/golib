package email

import (
	"github.com/jordan-wright/email"
	"net/smtp"
)

type SendMessage struct {
	//标题
	Subject string `json:"subject"`
	//内容
	Body string `json:"body"`
	//附件文件路径
	Attachments []string `json:"attachments"`
	//收件人
	To []Address `json:"to"`
	//抄送
	CC []Address `json:"cc"`
	//头信息
	Headers map[string]string `json:"headers"`
}

func Send(account *Account, message *SendMessage) error {
	e := email.NewEmail()
	e.From = account.Email
	if len(message.To) > 0 {
		for _, item := range message.To {
			e.To = append(e.To, item.Email)
		}
	}
	if len(message.CC) > 0 {
		for _, item := range message.CC {
			e.Cc = append(e.Cc, item.Email)
		}
	}
	e.Subject = message.Subject
	e.HTML = []byte(message.Body)
	//加入头信息
	if len(message.Headers) > 0 {
		for key, item := range message.Headers {
			e.Headers.Set(key, item)
		}
	}
	//加入附件
	if len(message.Attachments) > 0 {
		for _, item := range message.Attachments {
			_, err := e.AttachFile(item)
			if err != nil {
				return err
			}
		}
	}
	connection, err := account.SmtpConnection()
	if err != nil {
		return err
	}
	auth := smtp.PlainAuth("", account.Email, account.Password, connection.Host)
	return e.Send(connection.Host+":"+connection.Port, auth)
}
