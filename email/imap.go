package email

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/axgle/mahonia"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/charset"
	"github.com/emersion/go-message/mail"
	"github.com/gookit/goutil/fsutil"
	"github.com/gookit/goutil/strutil"
	"github.com/gookit/goutil/timex"
	"io"
	"log"
	"mime"
	"os"
	"strings"
)

type ImapService struct {
	client *client.Client
}

// Login 登录邮箱
func (service *ImapService) Login(account Account) error {
	connection, err := account.ImapConnection()
	if err != nil {
		return err
	}
	service.client, _ = client.DialTLS(fmt.Sprintf("%v:%v", connection.Host, connection.Port), nil)
	return service.client.Login(account.Email, account.Password)
}

// Logout 登出
func (service *ImapService) Logout() error {
	return service.client.Logout()
}

// Folders 获取邮箱文件夹
func (service *ImapService) Folders() []*Folder {
	mailBoxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- service.client.List("", "*", mailBoxes)
	}()
	folders := make([]*Folder, 0)
	for m := range mailBoxes {
		if len(m.Attributes) > 0 {
			for key, attr := range m.Attributes {
				m.Attributes[key] = strings.TrimLeft(attr, "\\")
			}
		}
		folders = append(folders, &Folder{
			Attributes: m.Attributes,
			Delimiter:  m.Delimiter,
			Name:       m.Name,
			AliasName:  service.folderAliasName(m.Name),
		})
	}
	return folders
}

// folderAliasName 获取文件夹映射别名
func (service *ImapService) folderAliasName(name string) string {
	if _, ok := FolderMapping[name]; ok {
		return FolderMapping[name]
	} else {
		return name
	}
}

// GetFolderMessages 收取邮件
func (service *ImapService) GetFolderMessages(folder *Folder, from uint32, limit uint32) ([]*Message, error) {
	result := make([]*Message, 0)
	// Select INBOX
	mbox, err := service.client.Select(folder.Name, false)
	if err != nil {
		return result, err
	}
	// 获取文件夹内的邮件，邮件为空时
	if mbox.Messages == 0 {
		return result, errors.New("No message in mailbox")
	}
	seqSet := new(imap.SeqSet)
	//to := mbox.Messages
	seqSet.AddRange(from, from+limit)
	// Get the whole message body
	section := &imap.BodySectionName{}
	items := []imap.FetchItem{section.FetchItem(), imap.FetchEnvelope, imap.FetchInternalDate, imap.FetchUid, imap.FetchFlags, imap.FetchRFC822}
	messages := make(chan *imap.Message, limit)

	done := make(chan error, 1)
	go func() {
		done <- service.client.UidFetch(seqSet, items, messages)
	}()
	for msg := range messages {
		r := msg.GetBody(section)
		if r == nil {
			log.Fatal("Server didn't returned message body")
		}
		//m, err := mail.CreateReader(r)
		mc, err := charset.Reader("gbk", r)
		if err != nil {
			log.Fatal(err)
		}
		m, err := mail.CreateReader(mc)
		if err != nil {
			log.Fatal(err)
		}
		item := &Message{
			Uid:         msg.Uid,
			MessageId:   msg.Envelope.MessageId,
			Date:        msg.Envelope.Date.Format(timex.DatetimeLayout),
			Subject:     service.decode(msg.Envelope.Subject),
			CC:          make([]*Address, 0),
			To:          make([]*Address, 0),
			Attachments: make([]*Attachment, 0),
			Headers:     make(map[string]string),
		}
		//是否已读
		for _, flag := range msg.Flags {
			if flag == imap.SeenFlag {
				item.Seen = true
			}
			if flag == imap.AnsweredFlag {
				item.Answered = true
			}
			if flag == imap.DeletedFlag {
				item.Deleted = true
			}
			if flag == imap.DraftFlag {
				item.Draft = true
			}
		}
		//发件人
		if len(msg.Envelope.From) > 0 {
			item.From = &Address{
				Name:  service.decode(msg.Envelope.From[0].PersonalName),
				Email: msg.Envelope.From[0].Address(),
			}
		}
		//收件人
		if len(msg.Envelope.To) > 0 {
			for _, toItem := range msg.Envelope.To {
				item.To = append(item.To, &Address{
					Name:  service.decode(toItem.PersonalName),
					Email: toItem.Address(),
				})
			}
		}
		//抄送人
		if len(msg.Envelope.Cc) > 0 {
			for _, ccItem := range msg.Envelope.Cc {
				item.CC = append(item.CC, &Address{
					Name:  service.decode(ccItem.PersonalName),
					Email: ccItem.Address(),
				})
			}
		}
		//邮件正文内容
		for {
			part, err := m.NextPart()
			if err == io.EOF {
				break
			}
			if err == nil {
				switch h := part.Header.(type) {
				//正文内容
				case *mail.InlineHeader:
					body, _ := io.ReadAll(part.Body)
					item.Body = string(body)
					//获取邮件头信息
					if len(m.Header.Map()) > 0 {
						for key, header := range m.Header.Map() {
							if len(header) > 0 {
								item.Headers[key] = header[0]
							}
						}

					}
				//附件
				case *mail.AttachmentHeader:
					fileName, err := h.Filename()
					if err != nil || fileName == "" {
						continue
					}
					//写入文件
					folder := "./static/" + strutil.MD5(item.MessageId)
					_ = os.MkdirAll(folder, os.ModePerm)
					filePath := folder + "/" + fileName
					body, _ := io.ReadAll(part.Body)
					if !fsutil.FileExists(filePath) {
						err = fsutil.WriteFile(filePath, body, os.ModePerm)
						if err != nil {
							return result, err
						}
					}
					contentType, _, _ := h.ContentType()
					//加入附件
					item.Attachments = append(item.Attachments, &Attachment{
						FileName: fileName,
						MimeType: contentType,
						Size:     h.Len(),
						Path:     strings.TrimLeft(filePath, "."),
					})
				}
			}
		}
		result = append(result, item)
	}
	if err := <-done; err != nil {
		log.Fatal(err)
	}
	return result, nil
}

// GetFolderMessageFlag 同步邮件状态
func (service *ImapService) GetFolderMessageFlag(folder *Folder) ([]*MessageFlag, error) {
	result := make([]*MessageFlag, 0)
	mbox, err := service.client.Select(folder.Name, false)
	if err != nil {
		return result, err
	}
	to := mbox.Messages
	// 获取文件夹内的邮件，邮件为空时
	if mbox.Messages == 0 {
		return result, errors.New("No message in mailbox")
	}
	messages := make(chan *imap.Message, to)
	seqSet := new(imap.SeqSet)
	seqSet.AddRange(1, to)
	done := make(chan error, 1)
	go func() {
		done <- service.client.Fetch(seqSet, []imap.FetchItem{imap.FetchUid, imap.FetchEnvelope,imap.FetchFlags}, messages)
	}()
	for msg := range messages {
		messageFlag := &MessageFlag{
			Uid:       msg.Uid,
			MessageId: msg.Envelope.MessageId,
			Subject:   service.decode(msg.Envelope.Subject),
			Date:      msg.Envelope.Date.Format(timex.DatetimeLayout),
		}
		//是否已读
		for _, flag := range msg.Flags {
			if flag == imap.SeenFlag {
				messageFlag.Seen = true
			}
			if flag == imap.AnsweredFlag {
				messageFlag.Answered = true
			}
			if flag == imap.DeletedFlag {
				messageFlag.Deleted = true
			}
			if flag == imap.DraftFlag {
				messageFlag.Draft = true
			}
		}
		result = append(result, messageFlag)
	}
	if err := <-done; err != nil {
		log.Fatal(err)
	}
	return result, nil
}

// SetRead 设置为已读
func (service *ImapService) SetRead(folderName string, uid uint32) error {
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(uid)
	_, err := service.client.Select(folderName, false)
	if err != nil {
		return err
	}
	item := imap.FormatFlagsOp(imap.AddFlags, true)
	flags := []interface{}{imap.SeenFlag}
	err = service.client.UidStore(seqSet, item, flags, nil)
	return err
}

// SetUnRead 设置未读
func (service *ImapService) SetUnRead(uid uint32) error {
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(uid)
	item := imap.FormatFlagsOp(imap.RemoveFlags, true)
	flags := []interface{}{imap.SeenFlag}
	err := service.client.UidStore(seqSet, item, flags, nil)
	return err
}

// Delete 删除邮件
func (service *ImapService) Delete(folderName string, uid uint32) error {
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(uid)
	_, err := service.client.Select(folderName, false)
	if err != nil {
		return err
	}
	item := imap.FormatFlagsOp(imap.AddFlags, true)
	flags := []interface{}{imap.DeletedFlag}
	err = service.client.UidStore(seqSet, item, flags, nil)
	return err
}

// decode 字符解码 适用于 标题，发件人，收件人，抄送人
func (service *ImapService) decode(str string) string {
	dec := new(mime.WordDecoder)
	dec.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
		switch charset {
		case "gb2312", "gbk", "gb18030":
			content, err := io.ReadAll(input)
			if err != nil {
				return nil, err
			}
			utf8str := service.gbkToUtf8(string(content))
			t := bytes.NewReader([]byte(utf8str))
			return t, nil
		default:
			return nil, fmt.Errorf("unhandle charset:%s", charset)
		}
	}
	result, err := dec.Decode(str)
	if err != nil {
		return str
	}
	return result
}

// gbkToUtf8 字符转码 GBK -> UTF8
func (service *ImapService) gbkToUtf8(content string) string {
	srcCoder := mahonia.NewDecoder("gbk")
	srcResult := srcCoder.ConvertString(content)
	tagCoder := mahonia.NewDecoder("utf-8")
	_, cdata, _ := tagCoder.Translate([]byte(srcResult), true)
	result := string(cdata)
	return result
}
