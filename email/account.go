package email

import (
	"strings"
)

type Account struct {
	// 邮件地址
	Email string
	// 登录密码
	Password string
	// 账号类型
	AccountType string
}

var (
	AccountTypeTencent = "exmail.qq.com" //腾讯企业邮箱
)

type Connection struct {
	//邮箱host
	Host string
	//邮箱端口
	Port string
}

// getDomain 获取邮箱域名
func (account *Account) getDomain() string {
	data := strings.Split(account.Email, "@")
	if len(data) > 1 {
		return data[1]
	} else {
		return ""
	}
}

// SmtpConnection 获取SMTP协议连接信息
func (account *Account) SmtpConnection() (*Connection, error) {
	switch account.AccountType {
	//腾讯企业邮箱
	case AccountTypeTencent:
		return &Connection{
			Host: "smtp.exmail.qq.com",
			Port: "587",
		}, nil
	default:
		domain := account.getDomain()
		return &Connection{
			Host: "smtp." + domain,
			Port: "465",
		}, nil
	}
}

// ImapConnection  获取IMAP连接信息
func (account *Account) ImapConnection() (*Connection, error) {
	switch account.AccountType {
	//腾讯企业邮箱
	case AccountTypeTencent:
		return &Connection{
			Host: "imap.exmail.qq.com",
			Port: "993",
		}, nil
	default:
		domain := account.getDomain()
		return &Connection{
			Host: "imap." + domain,
			Port: "993",
		}, nil
	}
}
