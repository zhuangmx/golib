接收邮件
=========

```go
service := &email.ImapService{}
_ = service.Login(email.Account{
	Email:    "zhuangmx@sumifcc.com",
	Password: "password",
	AccountType: email.AccountTypeTencent,
})
folders := service.Folders()
for _, item := range folders {
	if item.Name == "INBOX" {
		service.GetFolderMessages(item, 1, 10)
	}
}
```
发送邮件
=========
```go
account := email.Account{
	Email:    "test001@rrscientific.com",
	Password: "password",
	AccountType: email.AccountTypeTencent,
}
err := email.Send(&account, &email.SendMessage{
	To: []email.Address{
		{Name: "test001", Email: "test001@rrscientific.com"},
	},
	Subject: "测试",
	Body:    "<html><body>测试邮件内容</body></html>",
	Attachments: []string{
		"./static/test/test.jpg",
	},
})
```
