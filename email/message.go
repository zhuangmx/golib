package email

type Message struct {
	//邮件UID
	Uid uint32
	//邮件ID
	MessageId string
	//邮件日期
	Date string
	//是否已读
	Seen bool
	//是否草稿
	Draft bool
	//是否删除
	Deleted bool
	//是否已回复
	Answered bool
	//发件人
	From *Address
	//收件人
	To []*Address
	//抄送人
	CC []*Address
	//标题
	Subject string
	//内容
	Body string
	//附件
	Attachments []*Attachment
	//头信息
	Headers map[string]string
}

type Address struct {
	//名称
	Name string
	//邮箱地址
	Email string
}

type Attachment struct {
	//附件名称
	FileName string
	//附件类型
	MimeType string
	//附件编码格式
	Encoding string
	//附件大小
	Size int
	//附件路径
	Path string
}

type MessageFlag struct {
	//邮件UID
	Uid uint32
	//邮件ID
	MessageId string
	//标题
	Subject string
	//邮件日期
	Date string
	//是否已读
	Seen bool
	//是否草稿
	Draft bool
	//是否删除
	Deleted bool
	//是否已回复
	Answered bool
}
