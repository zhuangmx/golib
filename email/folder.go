package email

type Folder struct {
	// 文件夹属性
	Attributes []string
	// 文件夹分隔符
	Delimiter string
	// 名称
	Name string
	// 转为中文的名称
	AliasName string
}

var FolderMapping = map[string]string{
	"Drafts":           "草稿",
	"Deleted Messages": "已删除邮件",
	"Junk":             "垃圾邮件",
	"INBOX":            "收件箱",
	"Sent Messages":    "已发送邮件",
}
