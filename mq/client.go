package mq

type MessageQueue interface {
	Push([]byte) error
	Consume(callback func(body string))
}

func NewClient() {
	
}
