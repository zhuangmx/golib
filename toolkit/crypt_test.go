package toolkit

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"math/big"
	"testing"
)

func Rand(min int64, max int64) int64 {
	if min == max {
		return min
	}
	n, err := rand.Int(rand.Reader, big.NewInt(max))
	if err != nil {
		panic(err)
	}
	val := n.Int64()
	if min+val > max {
		return val
	}
	return min + n.Int64()
}

func TestRandom(t *testing.T) {
	for i := 1; i <= 100; i++ {
		fmt.Printf("random number: %d\n", Rand(10, 100))
	}
}

func RandString(num int, str ...string) string {
	s := "0123456789"
	if len(str) > 0 {
		s = str[0]
	}
	var buf bytes.Buffer
	for i := 1; i <= num; i++ {
		index := int(Rand(0, int64(len(s))))
		buf.WriteString(s[index : index+1])
	}
	return buf.String()
}

func TestRandString(t *testing.T) {
	ss := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	fmt.Printf("random string:%s\n", RandString(20, ss))
}
