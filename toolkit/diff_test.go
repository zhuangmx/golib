package toolkit

import (
	"fmt"
	"testing"
)

func TestDiff(t *testing.T) {
	left := map[string]interface{}{
		"a": 1,
		"b": 2,
	}
	right := map[string]interface{}{
		"a": 1,
		"b": 3,
		"c": 4,
	}
	diff, _ := JsonCompare(left, right)
	fmt.Println(diff)
}
