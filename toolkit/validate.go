package toolkit

import (
	"fmt"
	english "github.com/go-playground/locales/en"
	zhongwen "github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"github.com/go-playground/validator/v10/translations/en"
	"github.com/go-playground/validator/v10/translations/zh"
	"reflect"
	"strings"
)

func Validate(locale string, data interface{}) error {
	if locale == "zh" {
		return validateTranslateZh(data)
	}
	return validateTranslateEn(data)
}

func validateTranslateZh(data interface{}) error {
	localeZh := zhongwen.New()
	uni := ut.New(localeZh, localeZh)
	trans, _ := uni.GetTranslator("zh")
	validate := validator.New()
	validate.RegisterTagNameFunc(func(field reflect.StructField) string {
		label := field.Tag.Get("label")
		if label != "" {
			return label
		}
		return field.Name
	})
	err := zh.RegisterDefaultTranslations(validate, trans)
	if err != nil {
		return err
	}
	err = validate.Struct(data)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		newTranslate := errs.Translate(trans)
		errResults := make([]string, 0)
		for _, item := range newTranslate {
			errResults = append(errResults, fmt.Sprintf("%v", item))
		}
		return fmt.Errorf(strings.Join(errResults, ";"))
	}
	return nil
}

func validateTranslateEn(data interface{}) error {
	localeEn := english.New()
	uni := ut.New(localeEn, localeEn)
	trans, _ := uni.GetTranslator("en")
	validate := validator.New()
	validate.RegisterTagNameFunc(func(field reflect.StructField) string {
		label := field.Tag.Get("json")
		if label != "" {
			return label
		}
		return field.Name
	})
	err := en.RegisterDefaultTranslations(validate, trans)
	if err != nil {
		return err
	}
	err = validate.Struct(data)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		newTranslate := errs.Translate(trans)
		errResults := make([]string, 0)
		for _, item := range newTranslate {
			errResults = append(errResults, fmt.Sprintf("%v", item))
		}
		return fmt.Errorf(strings.Join(errResults, ";"))
	}
	return nil
}
