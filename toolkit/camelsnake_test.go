package toolkit

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCamelString(t *testing.T) {
	assert.NotContains(t,CamelString("hello_world"),"_")
}

func TestSnakeString(t *testing.T) {
	assert.Contains(t,SnakeString("HelloWorld"),"_")
}