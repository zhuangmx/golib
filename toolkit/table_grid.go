package toolkit

func TableGrid(total int64, list interface{}) map[string]interface{} {
	return map[string]interface{}{
		"total": total,
		"list":  list,
	}
}
