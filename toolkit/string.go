package toolkit

import (
	"github.com/bwmarrin/snowflake"
	"github.com/satori/go.uuid"
	"regexp"
	"unicode"
	"unicode/utf8"
)

// LowerFirst lower first char
func LowerFirst(s string) string {
	if len(s) == 0 {
		return s
	}
	rs := []rune(s)
	f := rs[0]
	if 'A' <= f && f <= 'Z' {
		return string(unicode.ToLower(f)) + string(rs[1:])
	}
	return s
}

// UpperFirst upper first char
func UpperFirst(s string) string {
	if len(s) == 0 {
		return s
	}
	rs := []rune(s)
	f := rs[0]
	if 'a' <= f && f <= 'z' {
		return string(unicode.ToUpper(f)) + string(rs[1:])
	}
	return s
}

// Utf8Len of the string
func Utf8Len(s string) int {
	return utf8.RuneCount([]byte(s))
}

//判断字符串是否为中文[精确度需要反复试验]
func IsContainCN(str string) bool {
	var hzRegexp = regexp.MustCompile("[\u4e00-\u9fa5]+")
	return hzRegexp.MatchString(str)
}

//生成唯一ID
func UUID() string {
	return uuid.NewV4().String()
}

//生成新ID
var snowFlakeNode *snowflake.Node

func NewSnowflakeId() (string, error) {
	if snowFlakeNode == nil {
		node, err := snowflake.NewNode(1)
		if err != nil {
			return "", err
		}
		snowFlakeNode = node
	}
	// Generate a snowflake ID.
	id := snowFlakeNode.Generate()
	return id.String(), nil
}


