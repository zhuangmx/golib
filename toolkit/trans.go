package toolkit

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"reflect"
)

//结构体转map
func Struct2Map(obj interface{}) map[string]interface{} {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		fieldName := LowerFirst(t.Field(i).Name)
		//tag := t.Field(i).Tag.Get("json")
		//if tag != "" {
		//	fieldName = tag
		//}
		data[fieldName] = v.Field(i).Interface()
	}
	return data
}

func StructPoint2Map(obj interface{}) map[string]interface{} {
	mBytes, _ := json.Marshal(obj)
	var result map[string]interface{}
	_ = json.Unmarshal(mBytes, &result)
	return result
}

//拷贝对象
func DeepCopy(src, dst interface{}) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(src); err != nil {
		return err
	}
	return gob.NewDecoder(bytes.NewBuffer(buf.Bytes())).Decode(dst)
}
