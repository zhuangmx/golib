package http

import (
	"encoding/json"
	"errors"
	"github.com/imroc/req/v3"
	"github.com/valyala/fastjson"
)

type HttpClient struct {
	httpClient   *req.Client
	httpRequest  *req.Request
	httpResponse ClientResponse
	httpError    error
}

type ClientResponse interface {
	IsSuccess() bool
	GetContentType() string
	Bytes() []byte
	String() string
	GetStatusCode() int
	GetHeader(key string) string
}

func NewClient() *HttpClient {
	return &HttpClient{
		httpClient: req.C(),
	}
}

// Debug debug模式
func (client *HttpClient) Debug() *HttpClient {
	client.httpClient.DevMode()
	return client
}

// UserAgent 设置UA
func (client *HttpClient) UserAgent(ua string) *HttpClient {
	client.httpClient.SetUserAgent(ua)
	return client
}

// RandomUserAgent 随机UA
func (client *HttpClient) RandomUserAgent() *HttpClient {
	return client
}

// SetHeaders 设置请求头信息
func (client *HttpClient) SetHeaders(headers map[string]string) *HttpClient {
	client.httpClient.SetCommonHeaders(headers)
	return client
}

func (client *HttpClient) request() *req.Request {
	if client.httpRequest == nil {
		client.httpRequest = client.httpClient.R()
	}
	return client.httpRequest
}

// SetBodyString 设置body
func (client *HttpClient) SetBodyString(body string) *HttpClient {
	client.request().SetBodyString(body)
	return client
}

// SetBody 设置body
func (client *HttpClient) SetBody(body interface{}) *HttpClient {
	client.request().SetBody(body)
	return client
}

// SetQuery 设置url参数
func (client *HttpClient) SetQuery(params map[string]string) *HttpClient {
	client.request().SetQueryParams(params)
	return client
}

// SetForm 设置form data
func (client *HttpClient) SetForm(formData map[string]string) *HttpClient {
	client.request().SetFormData(formData)
	return client
}

// Get get请求
func (client *HttpClient) Get(url string) *HttpClient {
	response, err := client.request().Get(url)
	client.httpResponse = response
	client.httpError = err
	return client
}

// Post post请求
func (client *HttpClient) Post(url string) *HttpClient {
	response, err := client.request().Post(url)
	client.httpResponse = response
	client.httpError = err
	return client
}

// Put put请求
func (client *HttpClient) Put(url string) *HttpClient {
	response, err := client.request().Put(url)
	client.httpResponse = response
	client.httpError = err
	return client
}

// Delete delete请求
func (client *HttpClient) Delete(url string) *HttpClient {
	response, err := client.request().Delete(url)
	client.httpResponse = response
	client.httpError = err
	return client
}

// Response 返回数据
func (client *HttpClient) Response() (ClientResponse, error) {
	return client.httpResponse, client.httpError
}

// String 返回结果字符串
func (client *HttpClient) String() (string, error) {
	if client.httpError != nil {
		return "", client.httpError
	}
	return client.httpResponse.String(), nil
}

// Bytes 返回结果字节数组
func (client *HttpClient) Bytes() ([]byte, error) {
	if client.httpError != nil {
		return nil, client.httpError
	}
	return client.httpResponse.Bytes(), nil
}

// ToStruct 返回结果结构体
func (client *HttpClient) ToStruct(value interface{}) error {
	if client.httpError != nil {
		return client.httpError
	}
	err := json.Unmarshal(client.httpResponse.Bytes(), &value)
	return err
}

// ToDataStruct 获取数据结构体
func (client *HttpClient) ToDataStruct(data interface{}) error {
	if client.httpError != nil {
		return client.httpError
	}
	value, err := fastjson.ParseBytes(client.httpResponse.Bytes())
	if err != nil {
		return err
	}
	if string(value.GetStringBytes("code")) != "0" {
		message := string(value.GetStringBytes("message"))
		if message == "" {
			message = "服务器繁忙，请稍候重试"
		}
		return errors.New(message)
	}
	return json.Unmarshal([]byte(value.Get("data").String()), &data)
}

// ToDataList 获取数据列表
func (client *HttpClient) ToDataList(list interface{}) error {
	if client.httpError != nil {
		return client.httpError
	}
	value, err := fastjson.ParseBytes(client.httpResponse.Bytes())
	if err != nil {
		return err
	}
	if string(value.GetStringBytes("code")) != "0" {
		message := string(value.GetStringBytes("message"))
		if message == "" {
			message = "服务器繁忙，请稍候重试"
		}
		return errors.New(message)
	}
	val := value.Get("data").Get("list").String()
	return json.Unmarshal([]byte(val), &list)
}
