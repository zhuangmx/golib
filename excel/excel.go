package excel

import (
	"bytes"
	"fmt"
	"github.com/xuri/excelize/v2"
)

type MakeOptions struct {
	SheetName string
	Title     []Title
	Data      []map[string]interface{}
}

type Title struct {
	Name   string
	Field  string
	Format string
}

var CellFormatText = "text"

// 生成excel文件
func Make(options MakeOptions) ([]byte, error) {
	f := excelize.NewFile()
	// 创建一个工作表
	if options.SheetName == "" {
		options.SheetName = "Sheet1"
	}
	index, err := f.NewSheet(options.SheetName)
	if err != nil {
		return nil, err
	}
	// 设置工作簿的默认工作表
	f.SetActiveSheet(index)
	// 设置标题
	var columnIndex = 'A'
	for _, title := range options.Title {
		_ = f.SetCellValue(options.SheetName, fmt.Sprintf("%v%v", string(columnIndex), 1), title.Name)
		//设置内容
		row := 2
		for _, data := range options.Data {
			if cell, ok := data[title.Field]; ok {
				if title.Format == CellFormatText {
					_ = f.SetCellStr(options.SheetName, fmt.Sprintf("%v%v", string(columnIndex), row), fmt.Sprintf("%v", cell))
				} else {
					fmt.Println(fmt.Sprintf("%v%v", string(columnIndex), row))
					_ = f.SetCellValue(options.SheetName, fmt.Sprintf("%v%v", string(columnIndex), row), cell)
				}
			}
			row = row + 1
		}
		columnIndex = columnIndex + 1
	}
	buffer, _ := f.WriteToBuffer()
	return buffer.Bytes(), nil
}

// 读取excel文件
func Read(data []byte, start ...int) ([][]string, error) {
	result := make([][]string, 0)
	f, err := excelize.OpenReader(bytes.NewBuffer(data))
	if err != nil {
		return result, err
	}
	index := f.GetActiveSheetIndex()
	rows, err := f.GetRows(f.GetSheetName(index))
	if err != nil {
		return result, err
	}
	if len(start) > 0 {
		rows = rows[start[0]-1:]
	}
	return rows, nil
}
