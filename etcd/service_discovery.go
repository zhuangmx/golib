package etcd

import (
	"context"
	"go.etcd.io/etcd/api/v3/mvccpb"
	clientV3 "go.etcd.io/etcd/client/v3"
	"log"
	"sync"
	"time"
)

var discovery *ServiceDiscovery

// ServiceDiscovery 服务发现
type ServiceDiscovery struct {
	cli        *clientV3.Client  //etcd client
	serverList map[string]string //服务列表
	lock       sync.Mutex
}

func GetDiscovery() *ServiceDiscovery {
	if discovery == nil {
		discovery = NewServiceDiscovery()
	}
	return discovery
}

// NewServiceDiscovery  新建发现服务
func NewServiceDiscovery() *ServiceDiscovery {
	cli, err := InitClient()
	if err != nil {
		panic(any(err))
	}
	return &ServiceDiscovery{
		cli:        cli,
		serverList: make(map[string]string),
	}
}

// WatchService 初始化服务列表和监视
func (service *ServiceDiscovery) WatchService(prefix string) error {
	//根据前缀获取现有的key
	resp, err := service.cli.Get(context.Background(), prefix, clientV3.WithPrefix())
	if err != nil {
		return err
	}
	for _, ev := range resp.Kvs {
		service.SetServiceList(string(ev.Key), string(ev.Value))
	}
	//监视前缀，修改变更的server
	go func() {
		for {
			service.watcher(prefix)
			time.Sleep(10 * time.Second)
		}
	}()
	return nil
}

// watcher 监听前缀
func (service *ServiceDiscovery) watcher(prefix string) {
	rch := service.cli.Watch(context.Background(), prefix, clientV3.WithPrefix())
	for resp := range rch {
		for _, ev := range resp.Events {
			switch ev.Type {
			case mvccpb.PUT: //修改或者新增
				service.SetServiceList(string(ev.Kv.Key), string(ev.Kv.Value))
			case mvccpb.DELETE: //删除
				service.DelServiceList(string(ev.Kv.Key))
			}
		}
	}
}

// SetServiceList 新增服务地址
func (service *ServiceDiscovery) SetServiceList(key, val string) {
	service.lock.Lock()
	defer service.lock.Unlock()
	service.serverList[key] = string(val)
	log.Println("discovery put key :", key, "val:", val)
}

// DelServiceList 删除服务地址
func (service *ServiceDiscovery) DelServiceList(key string) {
	service.lock.Lock()
	defer service.lock.Unlock()
	delete(service.serverList, key)
	log.Println("discovery del key:", key)
}

// GetServiceList 获取服务地址列表
func (service *ServiceDiscovery) GetServiceList() map[string]string {
	service.lock.Lock()
	defer service.lock.Unlock()
	return service.serverList
}

// Close 关闭服务
func (service *ServiceDiscovery) Close() error {
	return service.cli.Close()
}
