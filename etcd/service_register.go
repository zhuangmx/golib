package etcd

import (
	"bitbucket.org/zhuangmx/golib/toolkit"
	"context"
	clientV3 "go.etcd.io/etcd/client/v3"
	"os"
	"strings"
)

var DefaultLease = int64(5)

type ServiceRegister struct {
	//etcd client
	cli *clientV3.Client
	//租约Id
	leaseId clientV3.LeaseID
	//租约 keepalive相应的chan
	keepAliveChan <-chan *clientV3.LeaseKeepAliveResponse
	//key
	key string
	//value
	value string
}

func NewServiceRegister() *ServiceRegister {
	cli, err := InitClient()
	if err != nil {
		panic(any(err.Error()))
	}
	port := "8080"
	if os.Getenv("SERVER_PORT") != "" {
		port = os.Getenv("SERVER_PORT")
	}
	if os.Getenv("REGISTER_PORT") != "" {
		port = os.Getenv("REGISTER_PORT")
	}
	value := "http://127.0.0.1:" + port
	if os.Getenv("REGISTER_HOST") != "" {
		host := os.Getenv("REGISTER_HOST")
		if !strings.Contains(host, "://") {
			host = "http://" + host
		}
		value = host + ":" + port
	}
	keyId, _ := toolkit.NewSnowflakeId()
	service := &ServiceRegister{
		cli:   cli,
		key:   DefaultKeyPrefix + os.Getenv("REGISTER_NAME") + "_" + keyId,
		value: value,
	}
	//申请租约设置时间keepalive并注册服务
	if err := service.putKeyWithLease(); err != nil {
		panic(any(err.Error()))
	}
	return service
}

// putKeyWithLease 设置租约
func (service *ServiceRegister) putKeyWithLease() error {
	//设置租约时间
	resp, err := service.cli.Grant(context.Background(), DefaultLease)
	if err != nil {
		return err
	}
	//注册服务并绑定租约
	_, err = service.cli.Put(context.Background(), service.key, service.value, clientV3.WithLease(resp.ID))
	if err != nil {
		return err
	}
	//设置租约 定期发送请求
	leaseRespChan, err := service.cli.KeepAlive(context.Background(), resp.ID)
	if err != nil {
		return err
	}
	service.leaseId = resp.ID
	service.keepAliveChan = leaseRespChan
	go service.ListenLeaseRespChan()
	return nil
}

// ListenLeaseRespChan 监听 续租情况
func (service *ServiceRegister) ListenLeaseRespChan() {
	for range service.keepAliveChan {
		//log.Println("续约成功:", leaseKeepResp)
	}
	go func() {
		_ = service.putKeyWithLease()
	}()
}

// Close 注销服务
func (service *ServiceRegister) Close() error {
	//撤销租约
	if _, err := service.cli.Revoke(context.Background(), service.leaseId); err != nil {
		return err
	}
	return service.cli.Close()
}
