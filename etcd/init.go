package etcd

import (
	"bitbucket.org/zhuangmx/golib/toolkit"
	"encoding/json"
	"errors"
	clientV3 "go.etcd.io/etcd/client/v3"
	"log"
	"os"
	"strings"
	"time"
)

var DefaultKeyPrefix = "rrs_"

func InitClient() (*clientV3.Client, error) {
	host := "127.0.0.1"
	port := "2379"
	if os.Getenv("ETCD_HOST") != "" {
		host = os.Getenv("ETCD_HOST")
	}
	if os.Getenv("ETCD_PORT") != "" {
		port = os.Getenv("ETCD_PORT")
	}
	endPoints := host + ":" + port
	return clientV3.New(clientV3.Config{
		Endpoints:   []string{endPoints},
		DialTimeout: 5 * time.Second,
	})
}

func RegisterAndListen() {
	NewServiceRegister()
	go func() {
		_ = GetDiscovery().WatchService(DefaultKeyPrefix)
	}()
	go func() {
		for {
			time.Sleep(30 * time.Second)
			services := GetDiscovery().GetServiceList()
			mBytes, _ := json.Marshal(services)
			log.Println("发现服务：", string(mBytes))
		}
	}()
}

// GetService 获取服务地址
func GetService(prefix string) (string, error) {
	services := GetDiscovery().GetServiceList()
	values := make([]string, 0)
	for key, value := range services {
		if strings.HasPrefix(key, DefaultKeyPrefix+prefix) {
			values = append(values, value)
		}
	}
	if len(values) <= 0 {
		return "", errors.New("service not register")
	} else if len(values) == 1 {
		return values[0], nil
	} else {
		random := toolkit.RandNumber(0, len(values)-1)
		return values[random], nil
	}
}

// GetUser 获取用户地址
func GetUser() (string, error) {
	return GetService(DefaultKeyPrefix + "user")
}

// GetGateway 获取网关服务
func GetGateway() (string, error) {
	return GetService(DefaultKeyPrefix + "gateway")
}
