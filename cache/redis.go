package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type Client struct {
	result []byte
	err    error
}

func NewClient() *Client {
	return &Client{}
}

func (client *Client) Connect() *redis.Client {
	host := "localhost"
	port := "6379"
	auth := ""
	index := 0
	if os.Getenv("REDIS_HOST") != "" {
		host = os.Getenv("REDIS_HOST")
	}
	if os.Getenv("REDIS_PORT") != "" {
		port = os.Getenv("REDIS_PORT")
	}
	if os.Getenv("REDIS_AUTH") != "" {
		auth = os.Getenv("REDIS_AUTH")
	}
	if os.Getenv("REDIS_DB") != "" {
		indexDb := os.Getenv("REDIS_DB")
		index, _ = strconv.Atoi(indexDb)
	}
	return redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%v:%v", host, port),
		Password: auth,
		DB:       index, // use default DB
	})

}

func (client *Client) Set(key string, value interface{}, timeout int64) error {
	valueOf := reflect.ValueOf(value)
	typeName := strings.ToLower(valueOf.Type().Name())
	var newValue interface{}
	if typeName == "string" || typeName == "int" || typeName == "int64" || typeName == "float64" {
		newValue = value
	} else {
		mBytes, err := json.Marshal(value)
		if err != nil {
			return err
		}
		newValue = string(mBytes)
	}
	rdb := client.Connect()
	ctx := context.Background()
	err := rdb.Set(ctx, key, newValue, time.Duration(timeout)*time.Second).Err()
	defer rdb.Close()
	return err
}

func (client *Client) Get(key string) *Client {
	rdb := client.Connect()
	defer rdb.Close()
	ctx := context.Background()
	client.result, client.err = rdb.Get(ctx, key).Bytes()
	return client
}

func (client *Client) Del(key string) error {
	rdb := client.Connect()
	defer rdb.Close()
	ctx := context.Background()
	return rdb.Del(ctx, key).Err()
}

func (client *Client) Keys(key string) ([]string, error) {
	rdb := client.Connect()
	defer rdb.Close()
	ctx := context.Background()
	return rdb.Keys(ctx, key).Result()
}

func (client *Client) ToInt64() (int64, error) {
	if client.err != nil {
		return 0, client.err
	}
	tmp, err := strconv.Atoi(string(client.result))
	return int64(tmp), err
}

func (client *Client) ToInt() (int, error) {
	if client.err != nil {
		return 0, client.err
	}
	return strconv.Atoi(string(client.result))
}

func (client *Client) ToFloat64() (float64, error) {
	if client.err != nil {
		return 0, client.err
	}
	return strconv.ParseFloat(string(client.result), 64)
}

func (client *Client) ToFloat32() (float32, error) {
	if client.err != nil {
		return 0, client.err
	}
	x, err := strconv.ParseFloat(string(client.result), 64)
	return float32(x), err
}

func (client *Client) ToString() (string, error) {
	if client.err != nil {
		return "", client.err
	}
	return string(client.result), nil
}

func (client *Client) ToStruct(data interface{}) error {
	if client.err != nil {
		return client.err
	}
	err := json.Unmarshal(client.result, &data)
	if err != nil {
		return err
	}
	return nil
}
