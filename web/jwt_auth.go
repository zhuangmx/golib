package web

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type JwtAuth struct {
	AuthCompanyId string `json:"authCompanyId"`
	AuthUserId    string `json:"authUserId"`
	AuthNickName  string `json:"authNickName"`
	AccountType   int    `json:"accountType"`
	CountryId     int64  `json:"countryId"`
	IsAuth        int    `json:"isAuth"`
	jwt.StandardClaims
}

// 秘钥
var JwtAuthKey = "bitbucket.org/zhuangmx/golib"

// 过期时间
var JwtExpireTime = 3600 * 24

// 生成token
func (jwtAuth *JwtAuth) CreateToken() string {
	jwtAuth.ExpiresAt = time.Now().Add(time.Duration(JwtExpireTime) * time.Second).Unix()
	jwtAuth.Issuer = "golib"
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtAuth)
	tokenStr, _ := token.SignedString([]byte(JwtAuthKey))
	return tokenStr
}

// 解析token
func (jwtAuth *JwtAuth) ParseToken(tokenStr string) error {
	token, err := jwt.ParseWithClaims(tokenStr, jwtAuth, func(token *jwt.Token) (interface{}, error) {
		return []byte(JwtAuthKey), nil
	})
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(*JwtAuth); ok && token.Valid {
		return nil
	} else {
		return err
	}
}
