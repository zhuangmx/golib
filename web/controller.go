package web

import (
	"bitbucket.org/zhuangmx/golib/toolkit"
	"github.com/gin-gonic/gin"
	"net/http"
)

func ResponseData(ctx *gin.Context, data interface{}) {
	ctx.JSON(http.StatusOK, toolkit.JSONResult{
		Code:    "0",
		Message: "success.",
		Data:    data,
	})
}

func ResponseError(ctx *gin.Context, err error) {
	se := err.(*ServerError)
	ctx.JSON(http.StatusOK, toolkit.JSONResult{
		Code:    se.Code,
		Message: se.Message,
	})
}

func Response(ctx *gin.Context, data interface{}, err error) {
	if err == nil {
		ResponseData(ctx, data)
	} else {
		ResponseError(ctx, err)
	}
}

func ResponseDataError(ctx *gin.Context, data interface{}, err error) {
	if err != nil {
		se := err.(*ServerError)
		ctx.JSON(http.StatusOK, toolkit.JSONResult{
			Code:    se.Code,
			Message: se.Message,
			Data:    data,
		})
	} else {
		ctx.JSON(http.StatusOK, toolkit.JSONResult{
			Code:    "0",
			Message: "success.",
			Data:    data,
		})
	}
}
