package web

import (
	"bitbucket.org/zhuangmx/golib/logs/sentry"
	"bitbucket.org/zhuangmx/golib/web/filters"
	"github.com/gin-gonic/gin"
)

var engine *gin.Engine

func GetEngine() *gin.Engine {
	if engine == nil {
		gin.SetMode(gin.DebugMode)
		sentry.Register()
		engine = gin.Default()
		engine.Use(filters.Recovery(), filters.AllowCors())
	}
	return engine
}
