package filters

import (
	"bitbucket.org/zhuangmx/golib/logs/sentry"
	"bitbucket.org/zhuangmx/golib/toolkit"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"runtime/debug"
)

func Recovery() func(context *gin.Context) {
	return func(context *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				stackMsg := string(debug.Stack())
				sentry.CaptureMessage(err.(string) + "\n" + stackMsg)
				context.Writer.WriteHeader(http.StatusInternalServerError)
				res := toolkit.JSONResult{
					Code:    "500",
					Message: err.(string),
				}
				mBytes, _ := json.Marshal(res)
				_, _ = context.Writer.Write(mBytes)
			}
		}()
		context.Next()
	}
}
