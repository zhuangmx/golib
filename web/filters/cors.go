package filters

import (
	toolkit2 "bitbucket.org/zhuangmx/golib/toolkit"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func AllowCors() gin.HandlerFunc {
	return func(context *gin.Context) {
		context.Header("Access-Control-Allow-Origin", "*")
		context.Header("Access-Control-Allow-Headers", "*")
		context.Header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, OPTIONS, DELETE")
		context.Header("Access-Control-Allow-Credentials", "true")
		context.Header("Cache-Control", "no-cache, private")
		if strings.ToUpper(context.Request.Method) == "OPTIONS" {
			context.JSON(http.StatusOK, toolkit2.JSONResult{
				Code:    "0",
				Message: "success.",
			})
			context.Abort()
			return
		}
		context.Next()
	}
}
