package web

const (
	ErrorForbid   = "403"
	ErrorArgs     = "422"
	ErrorInternet = "510"
	ErrorBusiness = "511"
)

type ServerError struct {
	Code    string
	Message string
}

func (serverError *ServerError) Error() string {
	return serverError.Message
}

//抛出异常
func ThrowError(code string, err error) error {
	return &ServerError{
		Code:    code,
		Message: err.Error(),
	}
}
