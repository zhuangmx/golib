package web

import "github.com/gin-gonic/gin"

type RequestContext struct {
	ctx         *gin.Context
	AccessToken string   `json:"accessToken"`
	ClientIp    string   `json:"clientIp"`
	AccessLang  string   `json:"accessLang"`
	JwtAuth     *JwtAuth `json:"jwtAuth"`
}

func GetRequestContext(ctx *gin.Context) RequestContext {
	auth := &JwtAuth{}
	_ = auth.ParseToken(ctx.GetHeader("AccessToken"))
	return RequestContext{
		ctx:         ctx,
		AccessToken: ctx.GetHeader("AccessToken"),
		ClientIp:    ctx.ClientIP(),
		AccessLang:  ctx.GetHeader("AccessLang"),
		JwtAuth:     auth,
	}
}
