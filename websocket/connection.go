package websocket

import (
	"bitbucket.org/zhuangmx/golib/toolkit"
	"encoding/json"
	"errors"
	"github.com/gorilla/websocket"
	"log"
	"sync"
	"time"
)

type Connection struct {
	wsSocket  *websocket.Conn // 底层websocket
	inChan    chan *protocol  // 读队列
	outChan   chan *protocol  // 写队列
	mutex     sync.Mutex      // 避免重复关闭管道,加锁处理
	isClosed  bool
	closeChan chan byte // 关闭通知
	id        string
	userId    string //绑定的用户ID
}

// 读取消息队列中的消息
func (connection *Connection) read() (*protocol, error) {
	select {
	case msg := <-connection.inChan:
		// 获取到消息队列中的消息
		return msg, nil
	case <-connection.closeChan:

	}
	return nil, errors.New("连接已经关闭")
}

// 写入消息到队列中
func (connection *Connection) write(messageType int, data []byte) error {
	select {
	case connection.outChan <- &protocol{messageType, data}:
	case <-connection.closeChan:
		return errors.New("连接已经关闭")
	}
	return nil
}

// 关闭连接
func (connection *Connection) close() {
	log.Println("关闭连接被调用了")
	_ = connection.wsSocket.Close()
	connection.mutex.Lock()
	defer connection.mutex.Unlock()
	if connection.isClosed == false {
		connection.isClosed = true
		// 删除这个连接的变量
		connections.Remove(connection.id)
		close(connection.closeChan)
	}
}

// 发送消息
func (connection *Connection) Send(message *Message) error {
	//加入发送监听，超时未返回回执需重新发送
	if message.Ack != MessageAck {
		listenMessages.Set(message.MessageId, listener{
			Message:    message,
			Connection: connection,
			ExpireAt:   time.Now().Add(10 * time.Second),
		})
	}
	mBytes, _ := json.Marshal(message)
	return connection.write(MessageType, mBytes)
}

// 处理队列中的消息
func (connection *Connection) ProcessLoop() {
	// 处理消息队列中的消息
	// 获取到消息队列中的消息，处理完成后，发送消息给客户端
	for {
		protocol, err := connection.read()
		if err != nil {
			log.Println("获取消息出现错误", err.Error())
			break
		}
		log.Println("接收到消息", string(protocol.data))
		// 读取消息体，执行业务
		message := &Message{}
		err = protocol.ToJson(message)
		if err != nil {
			break
		}
		if message.Ack == MessageAck {
			ackHandler(connection, message)
			//去除消息监听
			_, exists := listenMessages.Get(message.MessageId)
			if exists {
				listenMessages.Remove(message.MessageId)
			}
		} else {
			//添加消息回执 通知消息已收到
			ack := &Message{}
			_ = toolkit.DeepCopy(message, ack)
			ack.Ack = MessageAck
			_ = connection.Send(ack)
			if handle, ok := handlers[message.Code]; ok {
				handle(connection, message)
			} else {
				log.Println("未添加该消息处理业务：" + message.ToString())
			}
		}
	}
}

func (connection *Connection) ReadLoop() {
	_ = connection.wsSocket.SetReadDeadline(time.Now().Add(pongWait))
	for {
		// 读一个message
		msgType, data, err := connection.wsSocket.ReadMessage()
		if err != nil {
			websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure)
			log.Println("消息读取出现错误", err.Error())
			connection.close()
			return
		}
		req := &protocol{
			msgType,
			data,
		}
		// 放入请求队列,消息入栈
		select {
		case connection.inChan <- req:
		case <-connection.closeChan:
			return
		}
	}
}

func (connection *Connection) WriteLoop() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
	}()
	for {
		select {
		// 取一个应答
		case msg := <-connection.outChan:
			// 写给websocket
			if err := connection.wsSocket.WriteMessage(msg.messageType, msg.data); err != nil {
				log.Println("发送消息给客户端发生错误", err.Error())
				// 切断服务
				connection.close()
				return
			}
		case <-connection.closeChan:
			// 获取到关闭通知
			return
		case <-ticker.C:
			// 出现超时情况
			_ = connection.wsSocket.SetWriteDeadline(time.Now().Add(writeWait))
			if err := connection.wsSocket.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// 绑定登录用户ID
func (connection *Connection) Bind(userId string) {
	connection.userId = userId
}

// 获取绑定的用户ID
func(connection *Connection) GetBindUserId() string {
	return connection.userId
}
