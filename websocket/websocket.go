package websocket

import (
	"bitbucket.org/zhuangmx/golib/toolkit"
	"bitbucket.org/zhuangmx/golib/toolkit/cmap"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	connections    cmap.ConcurrentMap
	listenMessages cmap.ConcurrentMap
	handlers       = make(map[int]func(connection *Connection, message *Message))
	ackHandler     = func(connection *Connection, message *Message) {}
)

const (
	// 允许等待的写入时间
	writeWait = 10 * time.Second
	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second
	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

func handler(resp http.ResponseWriter, req *http.Request) {
	// 应答客户端告知升级连接为websocket
	wsSocket, err := upgrader.Upgrade(resp, req, nil)
	if err != nil {
		log.Println("升级为websocket失败", err.Error())
		return
	}
	connId, _ := toolkit.NewSnowflakeId()
	conn := &Connection{
		wsSocket:  wsSocket,
		inChan:    make(chan *protocol, 1000),
		outChan:   make(chan *protocol, 1000),
		closeChan: make(chan byte),
		isClosed:  false,
		id:        connId,
	}
	connections.Set(connId, conn)
	// 处理器,发送定时信息，避免意外关闭
	go conn.ProcessLoop()
	// 读协程
	go conn.ReadLoop()
	// 写协程
	go conn.WriteLoop()
}

func Start() {
	addrPort := "9000"
	if os.Getenv("WEBSOCKET_PORT") != "" {
		addrPort = os.Getenv("WEBSOCKET_PORT")
	}
	connections = cmap.New()
	listenMessages = cmap.New()
	go listenLoop()
	http.HandleFunc("/", handler)
	_ = http.ListenAndServe(":"+addrPort, nil)
}

// 添加消息分发处理
func AddHandler(code int, handle func(connection *Connection, message *Message)) {
	handlers[code] = handle
}

// 增加消息回执处理
func AddAckHandler(handle func(connection *Connection, message *Message)) {
	ackHandler = handle
}

func Broadcast(message *Message) {
	go func() {
		if connections == nil || connections.IsEmpty() {
			return
		}
		for _, key := range connections.Keys() {
			item, exists := connections.Get(key)
			if exists {
				_ = item.(*Connection).Send(message)
			}
		}
	}()
}

// 指定用户ID发送消息
func SendByUid(userId string, message *Message) {
	if connections != nil && connections.Count() > 0 {
		for _, item := range connections.Items() {
			conn := item.(*Connection)
			if conn.GetBindUserId() != userId {
				_ = conn.Send(message)
			}
		}
	}
}

// 监听发送数据 是否收到回执,超时未收到回执需重新发送
func listenLoop() {
	for {
		if listenMessages.Count() > 0 {
			for key, item := range listenMessages.Items() {
				listen := item.(listener)
				if listen.Connection.isClosed {
					listenMessages.Remove(key)
				} else {
					fmt.Println("未收到回执，重新发送：", listen.Message.Body)
					_ = listen.Connection.Send(listen.Message)
					listen.ExpireAt = time.Now().Add(10 * time.Second)
					listenMessages.Set(key, listen)
				}
			}
		}
		time.Sleep(10 * time.Second)
	}
}
