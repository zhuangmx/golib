package websocket

import (
	"bitbucket.org/zhuangmx/golib/toolkit"
	"encoding/json"
	"github.com/gorilla/websocket"
	"time"
)

var MessageType = websocket.BinaryMessage
var MessageAck = 1

type protocol struct {
	//websocket.TextMessage 消息类型
	messageType int
	data        []byte
}

func (message *protocol) ToJson(result interface{}) error {
	err := json.Unmarshal(message.data, &result)
	return err
}

type Message struct {
	MessageId string      `json:"message_id"`
	Code      int         `json:"code"`
	Ack       int         `json:"ack"` //消息回执 1-是 0-否
	Body      interface{} `json:"body"`
}

func (message *Message) GetBody(data interface{}) error {
	return toolkit.DeepCopy(message.Body, data)
}

func (message *Message) ToString() string {
	mBytes, _ := json.Marshal(message)
	return string(mBytes)
}

type listener struct {
	Message    *Message
	Connection *Connection
	ExpireAt   time.Time
}
