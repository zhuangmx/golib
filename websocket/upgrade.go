package websocket

import (
	"github.com/gorilla/websocket"
	"net/http"
)

var upgrader = websocket.Upgrader{
	// 允许所有的CORS 跨域请求
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}