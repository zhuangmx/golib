package sentry

import (
	"github.com/getsentry/sentry-go"
	"log"
	"os"
	"time"
)

func Register() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: os.Getenv("SENTRY_DSN"),
		// Set TracesSampleRate to 1.0 to capture 100%
		// of transactions for performance monitoring.
		// We recommend adjusting this value in production,
		TracesSampleRate: 1.0,
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
}

func CaptureMessage(message string) {
	defer sentry.Flush(2 * time.Second)
	sentry.CaptureMessage(message)
}
